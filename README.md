# arduino-ledflasher

Async responsive LED Flashing library for Arduino. Sometimes you want to flash
but don't want to block all that time with `delay()`. You probably never want
to use `delay()` but it's so easy for many cases. This library allows you to
flash forever:

    LEDFlasher flasher;
    flasher.begin(port);
    flasher.flash(); // will flash until
    flasher.stop();  // stop is called

Or you can determine a number of times to flash:

    LEDFlasher flasher;
    flasher.flash(5); // I'll flash 5 times then stop.

Don't forget to call `update()` in the loop, or nothing will ever happen.

    void loop() {
      flasher.update();
    }

Essentially got bored of reimplementing this every sketch I ever wrote.


